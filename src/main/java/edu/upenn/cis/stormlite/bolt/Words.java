package edu.upenn.cis.stormlite.bolt;

import java.util.ArrayList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class Words{
	@PrimaryKey
	public String word;
	public List<String> counts;
	
	public Words() {
		
	}
	
	public Words(String word) {
		this.word = word;
		this.counts = new ArrayList<String>();
	}
}