package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;


public class Storage {
	private Environment env;
	private EntityStore store;
	public Set<String>contentSeen = new HashSet<String>();
	
	static final Logger logger = LogManager.getLogger(Storage.class);
	public PrimaryIndex<String, Words> wTable;
	
	
	public Storage(String directory) {
		EnvironmentConfig envConf = new EnvironmentConfig();
		logger.info("directory is {}", directory);
		
		envConf.setAllowCreate(true);
		File dir = new File(directory);
		env = new Environment(dir, envConf);
		
		StoreConfig sc = new StoreConfig();
		sc.setAllowCreate(true);
		store = new EntityStore(env,"EntityStore", sc);
		wTable = store.getPrimaryIndex(String.class, Words.class);
		System.out.println("database has been set up");
	}

	
	public synchronized void close() {
		// TODO Auto-generated method stub
		if (env!=null) {
			env.close();
		}
		if (store!=null) {
			store.close();
		}
	} 
	

	public synchronized void sync() {
		// TODO Auto-generated method stub
		
		if (env!=null) {
			env.sync();
		}
		if (store!=null) {
			store.sync();
		} 
	}
	
}