package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	Fields myFields = new Fields();
	TopologyContext context = null;
	String out = "";
	FileWriter writer;
	BufferedWriter buffer;
	ConsensusTracker votesForEos;
    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    int votesNum=0;
	@Override
	public void cleanup() {
		// Do nothing

	}

	@Override
	public boolean execute(Tuple input) {
		if (!input.isEndOfStream()) {
			String key = input.getStringByField("key");
	        String value = input.getStringByField("value");
	        System.out.println(getExecutorId() + ": " + input.toString());
				try {
					buffer.write(key+","+ value+"\r\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("written "+ key+","+ value+" in output \r\n");
			if (context.results.size()<100) {
				context.results.add("(" + key+","+value+")");
			}
		} else {
			votesNum-=1;
			if (votesNum==0) {
				if (writer!=null && buffer!=null) {
					try {
						buffer.close();
						writer.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				context.setState(TopologyContext.STATE.DONE);
				WorkerServer.cluster.shutdown();
			}
		}
			
		return true;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// Do nothing
		this.context = context;
		out = WorkerServer.storeDir + "/" + stormConf.get("output");
		File dir = new File(out);
		dir.mkdirs();
		File oldFile =  new File(out+"/output.txt");
		if (oldFile.exists()) {
			oldFile.delete();
		}
		try {
			oldFile.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer = new FileWriter(out+"/output.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buffer =  new BufferedWriter(writer);
		
		int reduce = Integer.parseInt(stormConf.get("reduceExecutors"));
        int worker = WorkerHelper.getWorkers(stormConf).length;
        //we need all reducers and remote bolts to emit end of stream in order to write in output
        votesNum = reduce * 1 * (worker -1) + reduce;
//        votesForEos = new ConsensusTracker(votesNum);
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
