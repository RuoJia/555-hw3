package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.WordFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import spark.HaltException;
import edu.upenn.cis.stormlite.bolt.PrintBolt;

public class MasterServer {  
	static HashMap<String, HashMap<String, String>> workerInfo = new HashMap<String, HashMap<String, String>>();
	static HashMap<String, Long> lastReceivedTime = new HashMap<String, Long>();
	static Logger logger = LogManager.getLogger(MasterServer.class);
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
        	logger.info("in status");
            response.type("text/html");
            String res = "<html><head><title>Master</title></head>\n";
            res += "<body>";
            int i = 0;
            res += "<p>Ruo Jia(rj16)</p>";
            logger.info("workerInfo is {}", workerInfo);
            List<String> removing = new ArrayList<String>();
            for (String w: workerInfo.keySet()){
            	logger.info("worker is {}", w);
            	if (System.currentTimeMillis() - lastReceivedTime.get(w)>= 30000) {
            		removing.add(w);
            	} else {
            		//active worker, display
            		logger.info("display active worker");
            		res += i + ": " 
            				+ "port=" + workerInfo.get(w).get("port") + ", "
            				+ "status=" + workerInfo.get(w).get("status") + ", "
            				+ "job=" + workerInfo.get(w).get("job") + ", "
            				+ "keysRead=" + workerInfo.get(w).get("keysRead") + ", "
            				+ "keysWritten=" + workerInfo.get(w).get("keysWritten") + ", "
            				+ "results=" + workerInfo.get(w).get("results") + "\n";
            		i+=1;
            	}
            }
            for (String w: removing) {
            	workerInfo.remove(w);
            	lastReceivedTime.remove(w);
            }
            res += "\n";
            res += "<form method=\"POST\" action=\"/submitjob\">\r\n"
            		+ "Job Name: <input type=\"text\" name=\"jobname\"/><br/>\r\n"
            		+ "Class Name: <input type=\"text\" name=\"classname\"/><br/>\r\n"
            		+ "Input Directory: <input type=\"text\" name=\"input\"/><br/>\r\n" 
            		+ "Output Directory: <input type=\"text\" name=\"output\"/><br/>\r\n" 
            		+ "Map Threads: <input type=\"text\" name=\"map\"/><br/>\r\n"
            		+ "Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\r\n";
            res += "<input type=\"submit\" value=\"Submit\"/>"+ "</form>\r\n";
            res += "</body></html>";
            return res;
        });

    }
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		logger.info("Sending {} request to " + url.toString(), reqType);
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			logger.info("params are {}", parameters);
//			logger.info(conn.getResponseCode());
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else {
			conn.getOutputStream();
		}
		
		return conn;
    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn", Level.DEBUG);

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();

        // TODO: route handler for /workerstatus reports from the workers
        handleWorkerStatus();
        
//        get("/aftershut", (req, res)->{
//        	System.exit(0);
//        	return "";
//        });
//        
        get("/shutdown", (req, res)->{
        	res.type("text/html");
        	for (String w: workerInfo.keySet()) {
        		String dest = "http://" + w;
        		sendJob(dest, "GET", null, "shutdown", "");
        		logger.info("successfuly sending shutdown to {}", dest);
        	}
        	workerInfo.clear();
        	lastReceivedTime.clear();
        	Thread.sleep(2000);
        	System.exit(0);
        	return "shutting down in 2 sec";
        });
        
        
        postJob();
    }
    
    public static void handleWorkerStatus() {
    	get("/workerstatus", (request, response)->{
    		String ip =  request.ip();
    		String port = request.queryParams("port");
//    		logger.info("in worker status, from {}", port);
    		HashMap<String, String> info = new HashMap<String, String>();
    		info.put("job", request.queryParams("jobname"));
    		info.put("status", request.queryParams("status"));
    		info.put("keysRead", request.queryParams("keysRead"));
    		info.put("keysWritten", request.queryParams("keysWritten"));
    		info.put("results", request.queryParams("results"));
    		info.put("port",request.queryParams("port"));
    		String key = ip+":"+ port;
    		workerInfo.put(key, info);
//    		logger.info("workerInfo is {}", workerInfo);
//    		logger.info("adding worker {} with info {}", key, info);
    		lastReceivedTime.put(key, System.currentTimeMillis());
    		return "received";
    	});
    }
    
    public static void postJob() {
    	post("/submitjob", (request, response)->{
    		logger.info("in submitting job {}", request.queryParams("classname"));
    		response.type("text/html");
    		Config config = new Config();
    		String jobname = request.queryParams("jobname");
    		String inputDir = request.queryParams("input");
    		String outputDir = request.queryParams("output");
    		String mapNum = request.queryParams("map");
    		String redNum = request.queryParams("reduce");
    		String classname = request.queryParams("classname");
    		
    		//initialize config
    		String workerList = "[";
    		for (String w: workerInfo.keySet()) {
    			workerList += w;
    			workerList += ",";
    		}
    		if (!workerList.equals("[")) {
    			workerList.substring(0, workerList.length()-1);
    		}
    		workerList += "]";
    		
    		config.put("workerList", workerList);
    		config.put("job", jobname);
    		config.put("mapClass", classname);
    		config.put("reduceClass", classname);
    		config.put("input", inputDir);
    		config.put("output", outputDir);
    		config.put("mapExecutors", mapNum);
    		config.put("reduceExecutors", redNum);
    		config.put("spoutExecutors", "1");
    		logger.info("config is {}", config);
    		
    		//initialize topology
    		TopologyBuilder tpb = new TopologyBuilder();
    		
    		FileSpout fs = new WordFileSpout();
    		MapBolt mb = new MapBolt();
    		ReduceBolt rb = new ReduceBolt();
    		PrintBolt pb = new PrintBolt();
    		
    		tpb.setSpout("WORDSPOUT", fs, Integer.valueOf(config.get("spoutExecutors")));
    		tpb.setBolt("MAPBOLT", mb, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping("WORDSPOUT", new Fields("value"));
    		tpb.setBolt("REDUCEBOLT", rb, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping("MAPBOLT", new Fields("key"));
    		tpb.setBolt("PRINTBOLT", pb, 1).firstGrouping("REDUCEBOLT");
    		
    		Topology tp = tpb.createTopology();
    		WorkerJob job = new WorkerJob(tp, config);
    		ObjectMapper mapper = new ObjectMapper();
    		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
			try {
				String[] workers = WorkerHelper.getWorkers(config);
	
				int i = 0;
				for (String dest: workers) {
					logger.info("dest is {}", dest);
			        config.put("workerIndex", String.valueOf(i++));
			        logger.info("params are {}", mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job));
					if (sendJob(dest, "POST", config, "definejob", 
							mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job definition request failed");
					}
					logger.info("successfully sending definejob to worker at {}", dest);
				}
				for (String dest: workers) {
					if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job execution request failed");
					}
					logger.info("successfully sending runjob to worker at {}", dest);
				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		        System.exit(0);
			}  
			String res = "<html><body><h1>running mapreduce</h1>\n";
    		res += "<a href=\"/status\">Get back to /status page</a>" + "</body></html>";
    		return res;
    	});
    }
}

