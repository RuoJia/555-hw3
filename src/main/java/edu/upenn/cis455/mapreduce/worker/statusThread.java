package edu.upenn.cis455.mapreduce.worker;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.upenn.cis.stormlite.TopologyContext;

public class statusThread extends Thread {
	String masterAddr;
	int port;
	boolean active;
	WorkerServer ws;
	
	public statusThread(WorkerServer ws, String addr, int port) {
		this.masterAddr = addr;
		this.port = port;
		this.active = true;
		this.ws = ws;
	}
	
	public String listResults(ArrayList<String>results) {
		//parse results into a string
		int i = 0;
		String res = "[";
		for (String s: results) {
			res += s;
		}
		return res + "]";
	}
	
	public void run() {
		while (active) {
			String url = "http://" + this.masterAddr + "/workerstatus";
			TopologyContext ourContext = ws.currentContext;
			String jobname = ws.currentJob;
			if (jobname==null) {
				jobname = "NoJob";
			}
			//get status
			if (ourContext==null) {
				ws.status = "idle";
			} else {
				if (ourContext.getState()==TopologyContext.STATE.INIT) {
					ws.status = "waiting";
				} else if (ourContext.getState()==TopologyContext.STATE.MAP) {
					ws.status = "mapping";
				} else if (ourContext.getState()==TopologyContext.STATE.REDUCE) {
					ws.status = "reducing";
				} else {
					ws.status = "idle";
				}
			}
			
			// get keys read
			if (ws.status == "waiting") {
				ws.keysRead = ourContext.getMapOutputs()/Integer.valueOf(ws.currentConf.get("mapExecutors"));
			} else if (ws.status == "reducing") {
				ws.keysRead = ourContext.getReduceOutputs()/Integer.valueOf(ws.currentConf.get("reduceExecutors"));
			} else if (ws.status == "mapping"){
				ws.keysRead = ourContext.getMapOutputs()/Integer.valueOf(ws.currentConf.get("mapExecutors"));
			} else {
				ws.keysRead = 0;
			}
			
			//get keys written
			
			if (ws.status == "waiting") {
				ws.keysWritten = ourContext.getMapOutputs()/Integer.valueOf(ws.currentConf.get("mapExecutors"));
			} else if (ws.status == "reducing") {
				ws.keysWritten = ourContext.getReduceOutputs()/Integer.valueOf(ws.currentConf.get("reduceExecutors"));
			} else if (ws.status == "mapping"){
				ws.keysWritten = ourContext.getMapOutputs()/Integer.valueOf(ws.currentConf.get("mapExecutors"));
			} else {
				if (ourContext == null) {
					ws.keysWritten = 0;
				} else {
					ws.keysWritten = ourContext.getReduceOutputs()/Integer.valueOf(ws.currentConf.get("reduceExecutors"));
				}
			}
			
			//get results
			if (ourContext!=null) {
				ws.results = ourContext.results;
			} 
			
			//send request to master server
			String req = "port=" + port
					     + "&status=" + ws.status
					     + "&jobname=" + jobname
					     + "&keysRead=" + ws.keysRead
					     + "&keysWritten=" + ws.keysWritten
					     + "&results=" + listResults(ws.results);
			try {
				URL url2 = new URL(url+"?"+req);
				HttpURLConnection conn =  (HttpURLConnection) url2.openConnection();
				conn.setRequestMethod("GET");
				conn.setDoOutput(true);
				conn.getResponseCode();
//				WorkerServer.log.info("sending url {} to master with status {}", url+"?" + req, conn.getResponseCode());
			} catch (IOException e) {
				e.printStackTrace();
				try {
					WorkerServer.shutdown();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			//periodic sending request
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}