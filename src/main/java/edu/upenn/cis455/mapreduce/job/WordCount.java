package edu.upenn.cis455.mapreduce.job;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;

public class WordCount implements Job {

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
	static Set<String> keySeen = new HashSet<String>();
	public void map(String key, String value, Context context, String sourceExecutor) {
        String[] toks = value.split("\\s+");
        for (String word : toks) {
            context.write(word, "1", sourceExecutor);
        }
    }

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
    public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor) {
        int sum = 0;
        while(values.hasNext()) {
            sum += Integer.parseInt(values.next());
        }
        if (keySeen.contains(key)) {
        	return;
        }
        context.write(key, "" + sum, sourceExecutor);
        keySeen.add(key);
    }

}
